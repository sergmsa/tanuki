<?php


interface Handler
{
    public function setNext(Handler $handler): Handler;

    public function getPrice(string $pair): ?float;
}

abstract class Request implements Handler
{
    /**
     * Ссылка на следующий класс для вызова по цепочке
     * @var Handler
     */
    private Handler $nextHandler;

    /**
     * Устанавливаем ссылку на следующий класс для вызова по цепочке
     * @param Handler $handler
     * @return Handler
     */
    public function setNext(Handler $handler): Handler
    {
        $this->nextHandler = $handler;
        return $handler;
    }

    /**
     * Получение стоимости валюты
     * @param string $pair Валютная пара
     * @return float|null
     */
    public function getPrice(string $pair): ?float
    {
        if ($this->nextHandler) {
            return $this->nextHandler->getPrice($pair);
        }
    }
}

class Cache extends Request
{
    private \Redis $connection;

    /**
     * Cache constructor.
     * @param \Redis $connection
     */
    public function __construct(\Redis $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Запрос в кеш(например Redis) для получения курса валюты
     * @param string $pair Валютная пара
     * @return float|null
     */
    private function requestCache(string $pair): ?float
    {
        // Тут идет обработка запроса в Redis через $this->connection
    }

    public function getPrice(string $pair): ?float
    {
        $price = $this->requestCache($pair);

        if (isset($price)) {
            echo "Для валютной пары {$pair} найдено в кеше:\n";
            return $price;
        } else {
            return parent::getPrice($pair);
        }
    }
}

class Db extends Request
{
    private \PDO $connection;

    /**
     * Db constructor.
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Запрос в базу данных для получения курса валюты
     * @param string $pair Валютная пара
     * @return float|null
     */
    private function requestDb(string $pair): ?float
    {
        // Тут идет обработка запроса в базу данных через $this->connection
    }

    public function getPrice(string $pair): ?float
    {
        $price = $this->requestDb($pair);

        if (isset($price)) {
            echo "Для валютной пары {$pair} найдено в базе:\n";
            return $price;
        } else {
            return parent::getPrice($pair);
        }
    }
}

class Http extends Request
{
    private $connection;

    /**
     * Http constructor.
     * @param $connection
     */
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Запрос во внешнюю среду для получения курса валюты
     * @param string $pair Валютная пара
     * @return float|null
     */
    private function requestHttp(string $pair): ?float
    {
        // Тут идет обработка запроса в к внешнему источнику через $this->connection
    }

    public function getPrice(string $pair): ?float
    {
        $price = $this->requestHttp($pair);

        if (isset($price)) {
            echo "Для валютной пары {$pair} найдено во внешнем источнике:\n";
            return $price;
        } else {
            return parent::getPrice($pair);
        }
    }
}

$cache = new Cache();
$db = new Db();
$http = new Http();

$cache->setNext($db)->setNext($http);

echo $cache->getPrice('USD_RUB')."\n";
echo $cache->getPrice('EUR_RUB')."\n";
echo $cache->getPrice('JPY_RUB')."\n";