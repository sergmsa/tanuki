<?php
require "predis/autoload.php";
Predis\Autoloader::register();

$client = new Predis\Client();

try {
    $client->connect();
} catch (Predis\Connection\ConnectionException $exception) {
    // We could not connect to Redis! Your handling code goes here.
}

print_r($client->info());